import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {render} from 'react-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux';
import AppRouter from './components/AppRouter.jsx';
import Reducer from './reducers/index.jsx';


const store = createStore(Reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

render(

	<Provider store={store}>
		<AppRouter />
	</Provider>,
	document.getElementById('root'),
);
	