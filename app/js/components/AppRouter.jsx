import React, {PureComponent} from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Provider } from 'react-redux';
import {render} from 'react-dom';
import { createStore } from 'redux'
import indexContainer from '../containers/indexContainer.jsx';
import AppContainer from '../containers/AppContainer.jsx';
import Reducer from '../reducers/index.jsx';


export default class AppRouter extends PureComponent {
  render(){
  	return (
	<Router>
      	<div>
        	<Route exact path="/" component={indexContainer} />
        	<Route path="/create" component={AppContainer} />
        	<Route path="/update/:id" component={AppContainer} />
     	</div>
     </Router>
  		)  
}
}