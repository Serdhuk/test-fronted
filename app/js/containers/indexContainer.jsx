import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import usersData from '../user/users.json';
import style from './style.scss';
	

class indexContainer extends PureComponent {
	
constructor(props) {
		super(props);

		this.deleteUser = this.deleteUser.bind(this);
		
	}

face(fio, keys) {
		if ("avatar" == keys) {
			return React.createElement(
				"img",
				{src:fio[keys]}
				)
		} else {
			return fio[keys]
		}
	}

updateUser(key) {
	//console.log	(this.props.store[key])
	 /*var allInputs = {},
	     output_arr = {}

	    allInputs = document.getElementsByClassName("input");
	    for (var values in allInputs) {
	      if (allInputs[values].name && allInputs[values].value) {
	        output_arr[allInputs[values].name] = allInputs[values].value;
	      }

	    }*/
		var action = {
			type: 'UPD_USER',
			user: key,
			/*payload:output_arr*/
		}
		this.props.dispatch(action);
	}

deleteUser(id) {
		var action = {
			type: 'DEL_USER',
			id: id
		};
		this.props.dispatch(action);
	}

flou(fio, index) {
		
		var Add = [],
			count = 0
		for (var keys in fio) {
			Add.push( React.createElement(
				"td",
				{key:count++},
				this.face(fio, keys)
				))
		}
		Add.push (
			<td key={count++}>
				<button className="Update"><Link to={`update/:id`} onClick={() => this.updateUser(index)}>Update</Link></button>
				<button className="Delete"><Link to="/" onClick={() => this.deleteUser(index)} >Delete</Link></button>
			</td>
		)
		return Add

	}

render() {
	return (
		<div>
			<div>
				
				<h1 className="Titul">
			

				<Link to="/create" className="Create">Create</Link>
			
					Titul
				</h1>

			</div>
				<table>
					<tbody>
						{
							this.props.store.map(
								(fio, index) => (
									<tr key={index}>{this.flou(fio, index)}</tr>
								)
							)
						}
					</tbody>
				</table>
		</div>
		)
	}
}

function mapStateToProps (state) {
  return {
    store: state
  }
}

export default connect(mapStateToProps)(indexContainer);


