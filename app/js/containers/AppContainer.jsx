import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import User from '../user/users.json';
import {render} from 'react-dom';
import Reducer from '../reducers/index.jsx';
import style from './style.scss';


class AppContainer extends PureComponent {
  constructor(props){
    super(props);
  const OutPut_state = {}
        User.map( value => {
          if( props.store[props.match.params.id]) {
            OutPut_state[value.name] = props.store[props.match.params.id][value.name] ? props.store[props.match.params.id][value.name]:''
          } else {
            OutPut_state[value.name] = ''
          }
        })
  const handleInput = (e) => {
  const name = e.target.name;

  const  value = e.target.value;

        this.setState({[value]:name}); 
      }

        this.state = OutPut_state;
        this.saveUser = this.saveUser.bind(this);
     
  }

 
switchFIO(fio) {
    switch (fio.type) {
                    
                    case 'select':
                       return  <select className="input" name={fio.name} onChange={this.handleInput} value={this.props[fio.name]}>{ fio.options.map((val, i) => {return <option key = { i } ref="gender"  value = { val.value } > { val.text } </option>})}</select> 
                    default:
                        return <input className="input" type={fio.type} name={fio.name} onChange={this.handleInput} value={this.props[fio.name]} />;
                    }
               }


saveUser() {
 var allInputs = {},
        OutPut = {}

    allInputs = document.getElementsByClassName("input");
    for (var values in allInputs) {
      if (allInputs[values].name && allInputs[values].value) {
        OutPut[allInputs[values].name] = allInputs[values].value;
      }
    }
    
    var action = {
      type: 'ADD_USER',
      user: OutPut
        } ;

        this.props.dispatch(action);

      }

render() {      
    return (
        <div>
          <div>
            <h1 className="Titul">
              SAVE DATA
            </h1>
          </div>
            <table>
              <tbody>{
                  User.map((fio,keys)=>
                    <tr  key={keys}>
                      <td>{fio.name}</td>
                      <td>
                        {this.switchFIO(fio)}
                      </td>
                    </tr>
                  )}
              </tbody>
          </table>
        <form> 
            <button type="submit" onClick={this.saveUser} className="btn btn-6 btn-6d"><Link to="/">Save</Link></button>
        </form>
        </div>
      );
  }

  }


  

function mapStateToProps (state) {
  return {
    store: state
  }
}



export default connect (mapStateToProps)(AppContainer);


